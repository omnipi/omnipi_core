@if "%SCM_TRACE_LEVEL%" NEQ "4" @echo off

:: ----------------------
:: KUDU Deployment Script
:: Version: 1.0.7
:: ----------------------

:: Prerequisites
:: -------------

:: Verify node.js installed
where node 2>nul >nul
IF %ERRORLEVEL% NEQ 0 (
  echo Missing node.js executable, please install node.js, if already installed make sure it can be reached from current environment.
  goto error
)

:: Setup
:: -----

setlocal enabledelayedexpansion

SET ARTIFACTS=%~dp0%..\artifacts
SET OP_ROOT=%~dp0%..
SET DEPLOYMENT_SOURCE=%~dp0%.\omnipi

IF NOT DEFINED DEPLOYMENT_TARGET (
  SET DEPLOYMENT_TARGET=%ARTIFACTS%\wwwroot
)

IF NOT DEFINED NEXT_MANIFEST_PATH (
  SET NEXT_MANIFEST_PATH=%ARTIFACTS%\manifest

  IF NOT DEFINED PREVIOUS_MANIFEST_PATH (
    SET PREVIOUS_MANIFEST_PATH=%ARTIFACTS%\manifest
  )
)

IF NOT DEFINED KUDU_SYNC_CMD (
  :: Install kudu sync
  echo Installing Kudu Sync
  call npm install kudusync -g --silent
  IF !ERRORLEVEL! NEQ 0 goto error

  :: Locally just running "kuduSync" would also work
  SET KUDU_SYNC_CMD=%appdata%\npm\kuduSync.cmd
)

:: OmniPi Directory Settings
IF NOT DEFINED OMNIPI_STORE (
  echo setting up the omnipi store location
  SET OMNIPI_STORE=%~dp0%..\store
  IF !ERRORLEVEL! NEQ 0 goto error
)

IF NOT DEFINED OMNIPI_LIBRARIES_SOURCE (
  echo setting up the omnipi libraries source location
  SET OMNIPI_LIBRARIES_SOURCE=%~dp0%.\libraries
  IF !ERRORLEVEL! NEQ 0 goto error
)
echo omnipi libraries source location %OMNIPI_LIBRARIES_SOURCE% now set

IF NOT DEFINED OMNIPI_LIBRARIES_TARGET (
  echo libraries target location is not defined..
  SET OMNIPI_LIBRARIES_TARGET=%~dp0%..\libraries
)
echo libraries target location is set to %OMNIPI_LIBRARIES_TARGET%

IF NOT EXIST "%OMNIPI_LIBRARIES_TARGET%" (
  echo Creating libraries target location
  MKDIR "%OMNIPI_LIBRARIES_TARGET%"
  IF !ERRORLEVEL! NEQ 0 goto error
) ELSE (
  echo Libraries target location already exists
)

IF NOT DEFINED OP_NEXT_MANIFEST_PATH (
  SET OP_NEXT_MANIFEST_PATH=%ARTIFACTS%\opmanifest

  IF NOT DEFINED OP_PREVIOUS_MANIFEST_PATH (
    SET OP_PREVIOUS_MANIFEST_PATH=%ARTIFACTS%\opmanifest
  )
)

IF NOT DEFINED WEBPACK_CMD (
  :: Install webpack
  echo Installing Webpack
  call npm install webpack -g --silent
  IF !ERRORLEVEL! NEQ 0 goto error

  :: Locally just running "kuduSync" would also work
  SET WEBPACK_CMD=%appdata%\npm\webpack.cmd
)

:: Install webpack
pushd "%DEPLOYMENT_TARGET%"
echo Installing all node deps
call npm install
IF !ERRORLEVEL! NEQ 0 goto error
popd

:: End OmniPi custom deployment

goto Deployment

:: Utility Functions
:: -----------------

:SelectPythonVersion

IF DEFINED KUDU_SELECT_PYTHON_VERSION_CMD (
  call %KUDU_SELECT_PYTHON_VERSION_CMD% "%DEPLOYMENT_SOURCE%" "%DEPLOYMENT_TARGET%" "%DEPLOYMENT_TEMP%"
  IF !ERRORLEVEL! NEQ 0 goto error

  SET /P PYTHON_RUNTIME=<"%DEPLOYMENT_TEMP%\__PYTHON_RUNTIME.tmp"
  IF !ERRORLEVEL! NEQ 0 goto error

  SET /P PYTHON_VER=<"%DEPLOYMENT_TEMP%\__PYTHON_VER.tmp"
  IF !ERRORLEVEL! NEQ 0 goto error

  SET /P PYTHON_EXE=<"%DEPLOYMENT_TEMP%\__PYTHON_EXE.tmp"
  IF !ERRORLEVEL! NEQ 0 goto error

  SET /P PYTHON_ENV_MODULE=<"%DEPLOYMENT_TEMP%\__PYTHON_ENV_MODULE.tmp"
  IF !ERRORLEVEL! NEQ 0 goto error
) ELSE (
  SET PYTHON_RUNTIME=python-2.7
  SET PYTHON_VER=2.7
  SET PYTHON_EXE=%SYSTEMDRIVE%\python27\python.exe
  SET PYTHON_ENV_MODULE=virtualenv
)

goto :EOF

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Deployment
:: ----------

:Deployment
echo Handling python deployment.

:: 1. KuduSync
IF /I "%IN_PLACE_DEPLOYMENT%" NEQ "1" (
  call :ExecuteCmd "%KUDU_SYNC_CMD%" -v 50 -f "%DEPLOYMENT_SOURCE%" -t "%DEPLOYMENT_TARGET%" -n "%NEXT_MANIFEST_PATH%" -p "%PREVIOUS_MANIFEST_PATH%" -i ".git;.hg;.deployment;deploy.cmd"
  IF !ERRORLEVEL! NEQ 0 goto error
)

:: OmniPi sync libraries..
:: 1.1 KuduSync
IF /I "%IN_PLACE_DEPLOYMENT%" NEQ "1" (
  call :ExecuteCmd "%KUDU_SYNC_CMD%" -v 50 -f "%OMNIPI_LIBRARIES_SOURCE%" -t "%OMNIPI_LIBRARIES_TARGET%" -n "%OP_NEXT_MANIFEST_PATH%" -p "%OP_PREVIOUS_MANIFEST_PATH%" -i ".git;.hg;.deployment;deploy.cmd"
  IF !ERRORLEVEL! NEQ 0 goto error
)

IF NOT EXIST "%DEPLOYMENT_TARGET%\requirements.txt" goto postPython
IF EXIST "%DEPLOYMENT_TARGET%\.skipPythonDeployment" goto postPython

echo Detected requirements.txt.  You can skip Python specific steps with a .skipPythonDeployment file.

:: 2. Select Python version
call :SelectPythonVersion

pushd "%DEPLOYMENT_TARGET%"

:: 3. Create virtual environment
IF NOT EXIST "%DEPLOYMENT_TARGET%\env\azure.env.%PYTHON_RUNTIME%.txt" (
  IF EXIST "%DEPLOYMENT_TARGET%\env" (
    echo Deleting incompatible virtual environment.
    rmdir /q /s "%DEPLOYMENT_TARGET%\env"
    IF !ERRORLEVEL! NEQ 0 goto error
  )

  echo Creating %PYTHON_RUNTIME% virtual environment.
  %PYTHON_EXE% -m %PYTHON_ENV_MODULE% env
  IF !ERRORLEVEL! NEQ 0 goto error

  copy /y NUL "%DEPLOYMENT_TARGET%\env\azure.env.%PYTHON_RUNTIME%.txt" >NUL
) ELSE (
  echo Found compatible virtual environment.
)

:: 4. Install packages
echo Pip install requirements.
env\scripts\pip install -r requirements.txt
IF !ERRORLEVEL! NEQ 0 goto error

REM Add additional package installation here
REM -- Example --
REM env\scripts\easy_install pytz
REM IF !ERRORLEVEL! NEQ 0 goto error

:: 5. Copy web.config
IF EXIST "%DEPLOYMENT_SOURCE%\web.%PYTHON_VER%.config" (
  echo Overwriting web.config with web.%PYTHON_VER%.config
  copy /y "%DEPLOYMENT_SOURCE%\web.%PYTHON_VER%.config" "%DEPLOYMENT_TARGET%\web.config"
)

popd



pushd "%OP_ROOT%"

:: 6a. Webpack the main omnipi files
IF /I "%IN_PLACE_DEPLOYMENT%" NEQ "1" (
  call :ExecuteCmd "%WEBPACK_CMD%" --config "webpack.config.js"
  IF !ERRORLEVEL! NEQ 0 goto error
)

:: 6b. Django collectstatic
IF EXIST "%DEPLOYMENT_TARGET%\manage.py" (
  IF EXIST "%DEPLOYMENT_TARGET%\env\lib\site-packages\django" (
    IF NOT EXIST "%DEPLOYMENT_TARGET%\.skipDjango" (
      echo Collecting Django static files. You can skip Django specific steps with a .skipDjango file.
      IF NOT EXIST "%DEPLOYMENT_TARGET%\static" (
        MKDIR "%DEPLOYMENT_TARGET%\static"
      )
      %DEPLOYMENT_TARGET%\env\scripts\python manage.py collectstatic --noinput --clear -i *.less
    )
  )
)

popd

:postPython

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
goto end

:: Execute command routine that will echo out when error
:ExecuteCmd
setlocal
set _CMD_=%*
call %_CMD_%
if "%ERRORLEVEL%" NEQ "0" echo Failed exitCode=%ERRORLEVEL%, command=%_CMD_%
exit /b %ERRORLEVEL%

:error
endlocal
echo An error has occurred during web site deployment.
call :exitSetErrorLevel
call :exitFromFunction 2>nul

:exitSetErrorLevel
exit /b 1

:exitFromFunction
()

:end
endlocal
echo Finished successfully.
