Running OmniPi on Azure
=======================

It is possible to run OmniPi on Azure web apps and the OmniPi homepage www.omnipi.org is currently hosted there.

Common Issues
-------------

Django/IIS do not serve static files

http://stackoverflow.com/questions/18799294/django-on-azure-not-loading-static-files

There appears to be an issue with Azure that if you have Python enabled in the Azure portal configuration for the site, then this will disrupt the web.config settings and not allow the /static/ folder to be by-passed. Simply setting Python to off in the portal and relying on runtime.txt to set the version of python to use, the site works as expected.