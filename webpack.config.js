var ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = {

	entry: [
		'./omnipi/omnipi/static/omnipi/scripts/omnipi.js', 
		'./omnipi/omnipi/static/omnipi/styles/omnipi.less'
	],
	output: {
		filename: './omnipi/omnipi/static/omnipi/scripts/bundle.js'
	},

	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				loader: 'babel-loader'
			},
			{
				test: /\.css$/,
				exclude: /node_modules/,
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
			},
			{
				test: /\.less$/,
				exclude: /node_modules/,
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader!less-loader')
			},
		]
	},

	resolve: ['.js', '.es6', '.jsx'],

	plugins: [
		new ExtractTextPlugin('./omnipi/omnipi/static/omnipi/styles/omnipi-min.css')
	]
}