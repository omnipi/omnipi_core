from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

import os

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', 'omnipi.views.index', name='home'),

    url(r'^markup$', 'omnipi.views.markup', name='markup example'),

    url(r'^promise$', 'omnipi.views.promise', name='promises'),

    # url(r'^omnipi/', include('omnipi.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),

    # url(r'^contacts/', include('contacts.urls')),
    
    # url(r'^mail/', include('mail.urls')),

    # url(r'^btc/', include('btc.urls')),

    # url(r'^music/', include('music.urls')),
    
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += patterns('django.contrib.staticfiles.views',
        url(r'^store/(?P<path>.*)$', settings.MEDIA_ROOT),
    )

# add in the modules
for module_file in os.listdir(os.path.join(settings.DIR, '../')):
    if module_file[:4] == 'mod_':
        module_name = module_file[4:]
        urlpatterns += patterns('', url(r'^' + module_name + '/', include(module_file + '.urls')))
