from django.db import models

# Create your models here.
class Email(models.Model):

	mssg_number = models.TextField()
	mssg_id = models.TextField(unique=True)

	from_addr = models.TextField()
	from_name = models.TextField(default="")

	to_addr = models.TextField()
	to_name = models.TextField(default="")
	cc_addr = models.TextField(default="")
	subject = models.TextField()
	email_date = models.DateTimeField()

	body = models.TextField(blank=True)
	text_body = models.TextField(blank=True)
	html_body = models.TextField(blank=True)
	
	attachments = models.TextField(default="")