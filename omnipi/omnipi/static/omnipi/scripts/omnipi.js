var omnipi = function() {

	function autoTextArea(area, minheight) {
		var shadowTA = document.createElement("div");
		var areaStyle = window.getComputedStyle(area);
		// we make the shadow element a little narrower since FF (and possibly other browsers) wrap textareas differently to divs
		shadowTA.style.width = '' + (parseInt(areaStyle.width) * 0.95) + 'px';
		shadowTA.style.fontSize = areaStyle.fontSize;
		shadowTA.style.fontFamily = areaStyle.fontFamily;
		shadowTA.style.marginLeft = '-1000px';
		area.parentElement.appendChild(shadowTA);
		area.onkeyup = function() {
			shadowTA.innerHTML = area.value.replace(/\n/g, "<br>.");
			var areaHeight = parseInt(areaStyle.height);
			var shadowHeight = parseInt(window.getComputedStyle(shadowTA).height);
			if (areaHeight < shadowHeight || areaHeight > (shadowHeight * 0.8)) {
				var newHeight = (shadowHeight * 1.001) > minheight ? shadowHeight*1.001 : minheight;
				area.style.height = '' + newHeight + 'px';
			}
		}
	}

	// opens the menu, sliding the main screen over and the menu coming in from the left
	function showMenu() {
		var menuElem = document.getElementById("op_main_menu");
		var menuClasses = menuElem.classList;
		var found = false;

		for (var i=0; i<menuClasses.length; i++) {
			if (menuClasses[i] === 'menu') {
				found = true;
				menuClasses.remove('menu');
				document.getElementById("op_maincontent").classList.remove('menu');
				i -= 1;
			}
		}
		if (!found) {
			menuClasses.add('menu');
			document.getElementById("op_maincontent").classList.add('menu');
		}
	}
}