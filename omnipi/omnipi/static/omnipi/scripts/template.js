/* given a template id (the id of the script tag) and a place in the parent page to put the output
this function will attempt to extract the template from the page, insert data and display it */
function renderTemplate(templateName, data, holder) {
	jQuery.ajax(omnipi.mail.emailTemplateStub + templateName + ".html", {
		dataType: "html",
		success: function(htmlData, status) {
			var htmlHolder = $("#"+holder);
			if (htmlHolder != null) {
				htmlHolder.html(htmlData);
				htmlHolder.html(parseTemplate(htmlHolder, data));
			}
			emlShow(holder);
		}
	});
}

/* part of a potential library of HTML rendering functions, but this takes a template element and
a data object and replaces all [[data-name]] areas where 'data-name' equates to a key in the data
object. */
function parseTemplate(template, data) {
	var fieldRE = /\[\[(\w*)\]\]/g;
	var content = template.html();
	var s;
	var newContent = [];
	var lastLastIndex = 0;
	// push the intervening HTML and any dynamic content
	while ((s = fieldRE.exec(content)) !== null) {
		newContent.push(content.substring(lastLastIndex, s.index));
		if (data[s[1]]) newContent.push(data[s[1]]);
		lastLastIndex = fieldRE.lastIndex;
	}
	// finally push the 'rest' of the template HTML, concat and return
	if (fieldRE.lastIndex != 0 || newContent.length == 0) {
		newContent.push(content.substring(fieldRE.lastIndex));
	}
	
	return newContent.join('');
}