from django.http import HttpResponse
from op_django.shortcuts import render, render_to_response
from django.core.urlresolvers import reverse
from django.conf import settings

import json
import imp

# import btc.views

def index(request):
    context = {}
    context['data'] = {}
    
    # populate the homepage with a list of apps that have been side loaded..
    context['op_apps'] = []
    for app in settings.OP_APPS:
        appdescription = 'No description is provided for the ' + app + ' module.'
        try:
            moduleinfo = imp.find_module('btc')  #app + '.views')
            if len(moduleinfo) == 3:
                print(app)
                appmodule = imp.load_module(app + '.views', moduleinfo[0], moduleinfo[1], moduleinfo[2])
                appdescription = appmodule.op_getinfo()
        except Exception as e:
            print(e)

        context['op_apps'].append({'title':app[4:], 'desc':appdescription, 'appname':app[4:], 'appurl':'/{}/'.format(app[4:])})

    # experiment with using the URI as the API and returning data based on the Accept header
    # if 'HTTP_ACCEPT' in request.META:
    #     accept = request.META.get('HTTP_ACCEPT', None)
    #     if accept == 'application/json':
    #         return render(request, 'omnipi/json.out', {'json': json.dumps(context)}, content_type="application/json")
    #     else:
    #         pass # fall through to the default HTML rendering

    return render(request, 'omnipi/index.html', context)
    # return HttpResponse(request, 'omnipi/empty.html', context)


def markup(request):
    # simply return a markup template
    context = {'AppName':'Markup Examples'}
    return render(request, 'omnipi/markup.html', context)

def promise(request):
    bodystring = ""
    if 'p' in request.GET:
        if request.GET['p'] == '0':
            bodystring = '{"Title":"Promises promises", "Blocks":[1,2,3,4,5]}'
        elif request.GET['p'] == '1':
            bodystring = '{"body":"this is the body of paragraph 1, it\'s not very interesting I\'m afraid"}'
        elif request.GET['p'] == '2':
            bodystring = '{"body":"this is now paragraph 2, and it\'s not getting any more interesting"}'
        elif request.GET['p'] == '3':
            bodystring = '{"body":"this is 3rd paragraph, and now we\'re getting pretty sad"}'
        elif request.GET['p'] == '4':
            bodystring = '{"body":"this is the body of paragraph 4, yes, same old same old"}'
        elif request.GET['p'] == '5':
            bodystring = '{"body":"this is the paragraph 5, it\'s the last one, notthing else to say except all done!"}'
        else:
            bodystring = '{"body":"unable to find a match for '+request.GET['p']+'"}'
        return render(request, 'omnipi/json.out', {'json': bodystring}, content_type="application/json")
    else:
        bodystring = '<script src="/static/omnipi/scripts/jquery/jquery-1.9.1.min.js"></script>'
        bodystring += '\n<script src="/static/omnipi/scripts/promise_test.js"></script>'
        bodystring += '\n<script>'
        bodystring += '\n    starttest();'
        bodystring += '\n</script>'
        bodystring += '\n<div id="promiseelem"></div>'
        return render(request, 'omnipi/just_body.html', {'body': bodystring})
        
#     return HttpResponse(content="""<html>
# <body>
# <a href="/contacts">Contacts</a><br>
# <a href="/mail">Email</a>
# </body>""")