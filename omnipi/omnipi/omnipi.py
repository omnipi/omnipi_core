# settings for omnipi modules
HTTP = {
	"proxy_url": 'proxy.example.com',
	"proxy_port": '80',
}

LOCALE = {
	'timezone_std': (8*3600),
	'timezone_dst': (7*3600),
}