import sqlite3
import csv
import sys
import datetime as dt


class dbClean():

	def __init__(self, dbLoc):
		print(dbLoc)
		self.conn = sqlite3.connect(dbLoc)

	def removeTable(self, tableName):
		c = self.conn.cursor()
		res = c.execute("DROP TABLE IF EXISTS " + tableName)
		for r in res:
			print(r)
		self.conn.commit()

	def showTables(self):
		c = self.conn.cursor()
		res = c.execute("SELECT * FROM sqlite_master")
		for r in res:
			print(r)

	def getTable(self, tableName):
		c = self.conn.cursor()
		res = c.execute("SELECT * FROM " + tableName)
		for r in res:
			print(r)

	def showSouth(self):
		c = self.conn.cursor()
		res = c.execute("SELECT * FROM south_migrationhistory")
		for r in res:
			print(r)

	def cleanSouth(self, appName):
		c = self.conn.cursor()
		res = c.execute("DELETE FROM south_migrationhistory WHERE app_name = ?", [appName])
		self.conn.commit()

	def cleanTable(self, tableName):
		c = self.conn.cursor()
		res = c.execute("DELETE FROM " + tableName)
		self.conn.commit()



if __name__ == "__main__":

	dataDAO = dbClean('./omnipi_general.db')

	# dataDAO.removeTable("_south_new_mail_email")
	# dataDAO.removeTable("mail_email")
	# dataDAO.cleanTable("mail_email")

	dataDAO.getTable("mail_email")

	# dataDAO.cleanSouth("mail")
	# dataDAO.showSouth()

	# dataDAO.showTables()