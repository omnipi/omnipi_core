# -*- coding: utf-8 -*-

# test maildir encoding
import csv
import sys
import datetime as dt
from email import message, utils, charset
import mailbox


# open the mail folder
mailbox.Maildir.colon = '!'
mailBox = mailbox.Maildir("~/maildir", factory=None)

charset.add_charset("utf-8", charset.SHORTEST, charset.QP)



m = message.Message()
m.add_header("To", "Nick", charset="utf-8")
m.add_header("From", "Dave", charset="utf-8")
m.add_header("Subject", "Hi", charset="utf-8")

m.add_header("Date", utils.formatdate(), charset="utf-8")
m.add_header("Message-ID", "TESTMail15783af4d00b", charset="utf-8")
m.add_header("Mime-version", "1.0", charset="utf-8")
m.add_header("Content-type", "multipart/alternative", charset="utf-8")

mText = message.Message()
# mText.set_charset("utf-8")
mText.add_header("Content-type", "text/plain", charset="utf-8")
mText.set_payload("This is a test mail for you to read", "utf-8")
m.attach(mText)

mHtml = message.Message()
# mHtml.set_charset("utf-8")
mHtml.add_header("Content-type", "text/html", charset="utf-8")
mHtml.set_payload("<body><p>This is a test \u00e4 email<br>for you to read</p></body>", "utf-8")
m.attach(mHtml)

# mailBox.add(m)
# print(m.as_string())
# print(sys.version)
# 

testout = open("c:\\utf8test.txt", "w", encoding="utf-8")
print(m.as_string(), file=testout)

try:
	print("here")
	print("Ç")
except Exception:
	print("first one failed")


sys.exit()
try:
	print("second try")
	print("Ç".encode("utf-8"))
except Exception:
	print("second one failed")
	# print(e)

try:
	print(str("Ç".encode("utf-8")))
except Exception:
	print("third one failed")
