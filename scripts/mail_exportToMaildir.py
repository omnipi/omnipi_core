import sqlite3, csv, sys, os
import datetime as dt

from email import message, charset
from email.header import Header
import email, mailbox, time


# open the mail folder
mailboxPath = os.path.join(os.path.curdir, "../store/mail")
mailbox.Maildir.colon = '!'
mailBox = mailbox.Maildir(mailboxPath, factory=None)


# get all mail from the db
conn = sqlite3.connect('../data/omnipi_general.db')
c = conn.cursor()
res = c.execute("SELECT * FROM mail_email")


# we prefer UTF-8 for all emails, with QP (Quoted Printable) encoding
charset.add_charset("utf-8", charset.SHORTEST, charset.QP)


# open the dump text file
testout = open("c:\\utf8test.txt", "w", encoding="utf-8")


count = []
cnt = 0

# dump it to screen
for r in res:
	# 0 = id number
	# 1 = empty
	# 2 = id
	# 3 = from
	# 4 = from_name
	# 5 = to
	# 6 = to_name ??
	# 7 = cc
	# 8 = Subject
	# 9 = Date
	# 10= body
	# 11= body plain
	# 12= body html
	# 13= attachments

	print(cnt)
	print(r[3])
	anyBody = False

	m = message.Message()
	m["To"] = Header(email.utils.formataddr((r[6], r[5])))
	m["From"] = Header(email.utils.formataddr((r[4],r[3])))
	m["Subject"] = Header(r[8], charset="utf-8")
	m.add_header("Message-ID", r[2])
	m.add_header("Mime-version", "1.0")
	m.add_header("Content-type", "multipart/alternative")

	# make the date format appropriate for RFC-2822
	datestring = time.strptime(r[9][:-7], "%Y-%m-%d %H:%M:%S")
	emailDateTime = time.mktime(datestring)
	m.add_header("Date", email.utils.formatdate(emailDateTime))

	if r[11] != '':
		mText = message.Message()
		mText.add_header("Content-type", "text/plain", charset="utf-8")
		mText.set_payload(r[11], "utf-8")
		m.attach(mText)
		anyBody = True

	if r[12] != '':
		mHtml = message.Message()
		mHtml.add_header("Content-type", "text/html", charset="utf-8")
		mHtml.set_payload(r[12], "utf-8")
		m.attach(mHtml)
		anyBody = True

	if anyBody:
		mailBox.add(m)
		print(m.as_string(), file=testout)
	else:
		print("nothing to do, both parts of the message are blank..")

	cnt += 1

print(count, "Errors")
