import django.shortcuts
import json


def render(request, template, context, **args):
    '''this overrides the general django render function, checking for the meta data that
    requests a JSON response and serves that instead...'''
    
    # experiment with using the URI as the API and returning data based on the Accept header
    if 'HTTP_ACCEPT' in request.META and request.META.get('HTTP_ACCEPT', None) == 'application/json':
        return django.shortcuts.render(request, 'omnipi/json.out', {'json': json.dumps(context)}, content_type="application/json")
    elif 'FORMAT' in request.GET and request.GET.get('FORMAT', None) == 'json':
        return django.shortcuts.render(request, 'omnipi/json.out', {'json': json.dumps(context)}, content_type="application/json")

    # fall through to the default HTML rendering
    return django.shortcuts.render(request, template, context, **args)


def render_to_response(**stuff):
    '''not sure what to do here'''


def redirect(**args):
    return django.shortcuts.redirect(**args)