import poplib
import socket
import socksipy.socks as socks # import sockssocket, PROXY_TYPE_SOCKS4, PROXY_TYPE_SOCKS5, PROXY_TYPE_HTTP

proxydata = {
    'host': 'proxy.example.com',
    'port': 80,
}

# since the poplib.POP3 class is likely defined the 'old' way, we must ensure that we inherit
# from the object class, so that calls to super will work and not throw a:
#       TypeError: must be type, not classobj
#
# We extend the poplib class and replace the __init__ method with one that creates a socks5
# proxy socket. The rest of the class is left alone.
class PopSocks(poplib.POP3, object):

    def __init__(self, host, port=poplib.POP3_PORT,
                 timeout=socket._GLOBAL_DEFAULT_TIMEOUT):
        self.sockshost = proxydata['host']
        self.socksport = proxydata['port']
        self.sock = socks.socksocket()

        socket.setdefaulttimeout(10.0)

        self.sock.setproxy(socks.PROXY_TYPE_SOCKS5, self.sockshost, self.socksport)
        print "setup the socks proxy settings"

        self.sock.connect((host, port))
        print "connected"
        
        self.file = self.sock.makefile('rb')
        self._debugging = 0
        self.welcome = self._getresp()
        # super(PopSocks, self).__init__(host, port, timeout)

    def _create_socket(self, timeout):
        # return self.sock.create_connection((self.host, self.port), timeout)
        self.sock.connect((self.host, self.port))

