from math import floor
import sys


codetable = {'initialised': False, 'data': {'enc': [], 'dec': {} } }


def buildtable():
    codetable['data']['enc'] = list('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/')
    for i, c in enumerate(codetable['data']['enc']):
        codetable['data']['dec'][c] = i


def encode(bain):
    if isinstance(bain, str):
        bain = bytearray(bain, "utf8")
    baout = []
    lowsize = bigsize = floor(len(bain) / 3)
    rem = len(bain) % 3
    if rem > 0: bigsize += 1

    for batch in range(bigsize):
        byte1 = bain[batch * 3]
        minibit1 = ((byte1 & 252) >> 2)
        minibit2 = minibit3 = minibit4 = -1

        if batch < lowsize:
            byte2 = bain[batch * 3 + 1]
            minibit2 = ((byte1 & 3) << 4) | ((byte2 & 240) >> 4)
            byte3 = bain[batch * 3 + 2]
            minibit3 = ((byte2 & 15) << 2) | ((byte3 & 192) >> 6)
            minibit4 = ((byte3) & 63)
        elif rem == 1:
            minibit2 = ((byte1 & 3) << 4)
        elif rem == 2:
            byte2 = bain[batch * 3 + 1]
            minibit2 = ((byte1 & 3) << 4) | ((byte2 & 240) >> 4)
            minibit3 = ((byte2 & 15) << 2)

        baout.append(lookup(minibit1))
        baout.append(lookup(minibit2))
        baout.append(lookup(minibit3))
        baout.append(lookup(minibit4))

    return "".join(baout)


def decode(encodedtext, debug=False):
    bain = list(encodedtext)
    baout = radixbytearray()
    blocks = floor(len(bain) / 4)
    if len(bain) % 4 != 0:
        raise Exception("encoded text MUST be a factor of 4 in length")

    for block in range(blocks):
        encchar1 = bain[block * 4]
        encchar2 = bain[block * 4 + 1]
        encchar3 = bain[block * 4 + 2]
        encchar4 = bain[block * 4 + 3]

        part1 = lookup(encchar1)
        part2 = lookup(encchar2)
        part3 = lookup(encchar3)
        part4 = lookup(encchar4)

        if debug:
            print('Part1', part1)
            print('Part2', part2)
            print('Part3', part3)
            print('Part4', part4)

        if part2 >= 0:
            baout.append((part1 << 2) | ((part2 & 48) >> 4))
            if part3 >= 0:
                baout.append(((part2 & 15) << 4) | ((part3 & 60) >> 2))
                if part4 >= 0:
                    baout.append(((part3 & 3) << 6) | part4)
                else:
                    baout.append(((part3 & 3) << 6))
            else:
                baout.append((part2 & 15) << 4)
        else:
            baout.append((part1 << 2))

        if debug:
            print(baout)

    return baout.array


def lookup(minibit):
    if not codetable['initialised']:
        buildtable()

    # print(type(minibit))

    if type(minibit) == int:
        if minibit == -1:
            return '='
        elif minibit < 0 | minibit >= 64:
            raise Exception("Byte out of bounds")
        else:
            return codetable['data']['enc'][minibit]
    else:
        if minibit in codetable['data']['dec']:
            # print(minibit)
            return codetable['data']['dec'][minibit]
        elif minibit == '=':
            return -2
        return -1


class radixbytearray(object):
    def __init__(self):
        self.array = bytearray()

    def append(self, bytevalue):
        if bytevalue != 0:
            self.array.append(bytevalue)


if __name__ == '__main__':
    print("sixSIX")
    print(list(bytearray("sixSIX", "utf8")))
    encoded = encode("sixSIX")
    print(encoded)
    decoded = decode(encoded)
    print(decoded.decode("utf8"))

    print('\nPure Binary Data Encoding')
    binary_data = b'\xd9\xc8\xa6\x0e>\t`\xb8\xc6/\xc8\xb7\x83\x06n\x92'
    binary_encoded = encode(binary_data)
    print('Original   ', bytearray(binary_data))
    print('Encoded    ', binary_encoded)
    print('Decoded    ', decode(binary_encoded))

    print("Welcome")
    encoded = encode("Welcome")
    print(encoded)
    decoded = decode(encoded)
    print(decoded)
