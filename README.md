OmniPi
======

Bring your data back inside, OmniPi is a personal cloud on a Raspberry Pi

It's your very own slice of the Pie

Getting Started
===============

OmniPi is built on [Python](http://www.python.org/) and [Django](http://djangoproject.org) and is intended to run on a 
[RaspberryPi](http://www.raspberrypi.org/) but in reality it'll run on any machine that has Python (3.4 for now).

To fire things up:

* Clone this repo
* Create a virtualenv for use with OmniPi (highly recommended)
* pip install -r requirements.txt
* python manage.py syncdb
* python manage.py runserver

The wiki now hosts a slightly improved set of installation instructions [https://bitbucket.org/omnipi/omnipi_core/wiki]

Right now the only thing we have working is mail but more functionality is coming soon! If you want to get involved 
with helping users regain control of their data contact us!


omnipi_core
===========

The core functionality of OmniPi: contacts and email (and maybe calendar too)